const functions = require("firebase-functions");
const express = require('express');
const admin = require('firebase-admin');
const bodyParser = require("body-parser");

admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

const app = express();
app.use(bodyParser.json());
app.get('/', async (req,res) => {
  try {
    const snapshot = await db.collection('user').get();
    const users = [];
    snapshot.forEach((doc) =>{
      users.push({
        id: doc.id,
        data: doc.data()
      });
    }
  );
  res.json(users);
  //users.push({id,...data});
  //res.send("Request Sent");
  //res.status(200).send(JSON.stringify(users));
  } catch (error) {
    res.status(500).send(error); 
  }
  
});

app.get("/:id", async (req, res) => {
  try {
    const userId = req.params.id;
    if(!userId) throw new Error('User ID is required');
    const snapshot = await db.collection('user').doc(userId).get();
    if(!snapshot.exists){
      throw new Error('User doesent exists');
    }
    res.json({
      id: snapshot.id,
      data: snapshot.data()
    });
  } catch (error) {
    res.status(500).send(error);
  }
  //const userData = snapshot.data();
  //res.status(200).send(JSON.stringify({id:userId, ...userData}));
});

app.post('/', async (req,res) => {
  try {
    const {firstname, lastname, email} = req.body;
    const data = {
      firstname,
      lastname,
      email
    }
    const users = await db.collection('user').add(data);
    const u = await users.get();
    res.json({
      id: users.id,
      data:users.data,
      message: "User Added Successfully"
    });
    //res.status(201).send('User added successfully');
  } catch (error) {
    res.status(500).send(error);
  }
});

app.put("/:id", async (req, res) => {
  try {
    const userId = req.params.id;
    const email = req.body.email;
    if(!userId) throw new Error('id is blank');
    if(!email) throw new Error('Email is required');
    const data = {
      email
    }
    const users = await db.collection('user').doc(userId).set(data, { merge: true});
    res.json({
      id: userId,
      data
    })
  } catch (error) {
    res.status(500).send(error); 
  }
  //const body = req.body;
  res.status(200).send("User Updated Successfully");
});

app.delete("/:id", async (req,res) => {
  try {
    const userId =req.params.id;
    if(!userId) throw new Error('id is blank');
    await db.collection("user").doc(userId).delete();
    res.json({
      id:userId,
    })
  } catch (error) {
    res.status(500).send(error);
  }
  //await admin.firestore().collection("user").doc(req.params.id).delete();
  //res.status(200).send("User Deleted Successfully");
});

exports.user = functions.https.onRequest(app);
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions.https.onRequest((request, response) => {
  functions.logger.info("Hello logs!", {structuredData: true});
  response.send("Hello from Firebase!");
});
